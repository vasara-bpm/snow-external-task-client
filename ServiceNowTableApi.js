const { logger } = require('camunda-external-task-client-js');

/*
    API to query tables from ServiceNow
*/
const RestClient = require('node-rest-client').Client;

//Environment variables

const snBaseUrl = process.env.SERVICENOWBASEURL || 'jyudev.service-now.com';
const snUsername = process.env.SNUSERNAME;
const snPassword = process.env.SNPASSWORD;

async function qyerySnTable(wlogger, tableName, keys){
    return new Promise((resolve, reject) => {
        
        // Initialize Rest Client
        var options_auth = { user: snUsername, password: snPassword };
        var restclient = new RestClient(options_auth);
        const reqHeaders = {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json',
        };

        //Set arguments
        var keyString = '';
        for (i in keys){
            if (i == 0){
                keyString += '?' + keys[i].keyName + keys[i].keyOperator + keys[i].keyValue;
            }else{
                keyString += '&' + keys[i].keyName + keys[i].keyOperator + keys[i].keyValue;
            }

        }

        var args = {
            path: {
                serviceNowBaseUrl: snBaseUrl,
                tableName: tableName,
                keyString: keyString,
            },
            headers: reqHeaders,

        };

        //Do the invoke
        wlogger.verbose("Requesting table information via rest API");
        restclient.get('https://${serviceNowBaseUrl}/api/now/table/${tableName}${keyString}', args, function(data, response) {
            wlogger.verbose('Received HTTP status: ' + response.statusCode);
            wlogger.verbose('Received HTTP response: ' + JSON.stringify(data));
            if(response.statusCode === 200 || response.statusCode === 201 || response.statusCode === 204){               
                resolve (data);
            }else if (response.statusCode === 400 || response.statusCode === 500){
                wlogger.error('Received HTTP status: ' + response.statusCode);
                var error = {
                    errorMessage: "Received HTTP Statuscode " + response.statusCode,
                    errorDetails: JSON.stringify(data),
                    retries: 0,
                }
                reject(error);
            }else{
                //Retriable fault
                wlogger.error('Received HTTP status: ' + response.statusCode + " Will retry.");
                var error = {
                    errorMessage: "Received HTTP Statuscode " + response.statusCode,
                    errorDetails: JSON.stringify(data),
                    retries: 1,
                }
                reject(error);
            }
        }).on('error', function (err) {
            wlogger.error('Request failed: ' + err.message);
            wlogger.verbose('Request options: ' + JSON.stringify(err.request.options));
            var error = {
                errorMessage: err.message,
                errorDetails: err.name,
                retries: 1,
            };
            reject(error);
        });
    });
};

async function fetchSnSysId(wlogger, tableName, keys)
{
        var data =  await qyerySnTable(wlogger, tableName, keys);
        wlogger.verbose('Data: ' + JSON.stringify(data));
        if(data.result[0] != undefined && data.result[0].sys_id != undefined){
            wlogger.info('Fetched Sys Id');
            wlogger.verbose(`sys_id: ${data.result[0].sys_id}`);
            return data.result[0].sys_id;
        }else {
            // Not found, need to throw an error
            var error = {
                errorMessage: 'Requested information not found',
                errorDetails: 'Table: ' + tableName + ', Keys: ' + JSON.stringify(keys),
                retries: 0,
            }
            throw error;
        }
}

module.exports = {fetchSnSysId, qyerySnTable}