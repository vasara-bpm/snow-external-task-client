/* 

Main module to start all Integration components in Vasara BPM - ServiceNow Integration

*/
global.requestHeartbeatTimestamp = new Date();
global.queryHeartbeatTimestamp = new Date();



const sendServiceRequestWorker = require('./SendServiceRequest').sendServiceRequestWorker();
const queryRequestStatusWorker = require('./QueryRequestStatus').queryRequestStatusWorker();
require('dotenv').config();
const winston = require('winston');
const { combine, timestamp, label, printf } = winston.format;
const http = require('http');

//Environemnt variables
const logLevel = process.env.LOGGING_LEVEL || 'info';
const monitorPort = process.env.MONITORPORT || 3000;
const heartbeatAlertLimit = process.env.HBALERTLIMIT || 3600;

//Create logger
const wloggerFormat = printf(({ level, message, label, timestamp }) => {
    return `${timestamp} [${label}] ${level}: ${message}`;
});

const logger = winston.createLogger({
    level: logLevel,
    format: combine(
      label({ label: 'ServiceNowExternalTaskClient' }),
      timestamp(),
      wloggerFormat,
    ),
    transports: [
      new winston.transports.Console(),
    ],
})

const listenMonitor = new Promise((resolve, reject) => {
  
  

  const hbRequestListener = function (req, res) {
    var sendHbDelta =  requestHeartbeatTimestamp.getTime() + heartbeatAlertLimit * 1000 - new Date().getTime();
    var queryHbDelta = queryHeartbeatTimestamp.getTime() + heartbeatAlertLimit * 1000 - new Date().getTime();
    if (sendHbDelta > 0 && queryHbDelta > 0){ 
      res.writeHead(200);
      res.end('{"status": "ok"}');
    }else{
      res.writeHead(500);
      res.end('{"status": "error"}');
    }
  }
  
  const server = http.createServer(hbRequestListener);
  server.listen(monitorPort);

});




Promise.all([sendServiceRequestWorker, queryRequestStatusWorker, listenMonitor]).then((values) => {

    logger.log(values);

}).catch(error => { 
    logger.log(error.message);
});

