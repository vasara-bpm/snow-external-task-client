/* 

Module reposible of creating request content

*/
var wlogger;
const serviceNowTableApi = require('./ServiceNowTableApi');

async function createServiceRequest(task, wloggerin){
    
    wlogger = wloggerin;
    wlogger.verbose('Processing tranfomation request');
    wlogger.verbose(`Request type: ${task.variables.get('serviceRequestType')}`);
    
    //Choose function to call
    switch (task.variables.get('serviceRequestType')){
        case 'DEV01':
            var msg = await createOrderItemsServiceRequest(task);
            return msg;
        default:
            var error = {
                errorMessage: "Unknown request type",
                errorDetails: "Transformation not specified from service request type: " + task.variables.get('serviceRequestType'),
                retries: 0,
            };
            throw error;
    }
}

async function createOrderItemsServiceRequest(task){
    
    var payload = JSON.parse(task.variables.get('payload'));
    
    var usrKeys = 
        [ 
            {
                keyName: 'user_name',
                keyOperator: '=',
                keyValue: payload.userid
            }
        ]
    

    var depKeys =
        [ 
            {
                keyName: 'id',
                keyOperator: '=',
                keyValue: payload.departmentcode
            }
        ]

    var usr_sys_id = await serviceNowTableApi.fetchSnSysId(wlogger, 'sys_user', usrKeys);
    var dep_sys_id = await serviceNowTableApi.fetchSnSysId(wlogger, 'cmn_department', depKeys);
    return {
        catalogitem_sys_id: 'ea40f7172f98601010ad9dacf699b6c8',
        requestbody: {
            "sysparm_quantity":  payload.qty,
            "variables":  {
                'user': usr_sys_id,
                "first_name":  payload.firstname,
                "last_name":  payload.lastname,
                "email": payload.email,
                "phone":  payload.phone,
                "additional_information":  payload.additional_information,
                "department":  dep_sys_id,
                "type_of_item":  payload.typeOfItem,
                "vasarakey" : payload.vasaraKey
            }
        }
    }
}

module.exports = {createServiceRequest};