/* 

Module reposible of sending service requests from Vasara BPM to ServiceNow

*/

const { Client, logger, Variables } = require('camunda-external-task-client-js');
require('dotenv').config();
const RestClient = require('node-rest-client').Client;
const winston = require('winston');
const CreateServiceRequest = require('./CreateServiceRequest');
const { combine, timestamp, label, printf } = winston.format;
const BearerTokenAuthInterceptor = require('vasara-bearer-token-interceptor');

//Environment variables
const logLevel = process.env.LOGGING_LEVEL || 'info';
const engineUrl = process.env.BPMENGINEURL || 'http://localhost:8080/engine-rest';
const topic = process.env.SENDREQUESTTOPIC || 'sendServiceRequest';
const snBaseUrl = process.env.SERVICENOWBASEURL || 'jyudev.service-now.com';
const taskClientTimeout = process.env.TASKCLIENTTIMEOUT || 10000;
const retryDelay = process.env.RETRYDELAY || 1000;
const workerId = process.env.SENDREQUESTWORKERID || 'some-random-id';
const authToken = process.env.AUTHORIZATION_TOKEN || '1234';
const snUsername = process.env.SNUSERNAME;
const snPassword = process.env.SNPASSWORD;

//Create a logger
const wloggerFormat = printf(({ level, message, label, timestamp }) => {
    return `${timestamp} [${label}] ${level}: ${message}`;
});

const wlogger = winston.createLogger({
    level: logLevel,
    format: combine(
      label({ label: 'SendServiceRequest' }),
      timestamp(),
      wloggerFormat,
    ),
    transports: [
      new winston.transports.Console(),
    ],
});


// Create Camunda task client configuration
//todo lisää interceptor
const config = { 
    baseUrl: engineUrl, 
    use: logger.level((logLevel ==='http') ? 'info' : logLevel), 
    asyncResponseTimeout: taskClientTimeout,
    workerId: workerId,
    interceptors: new BearerTokenAuthInterceptor(authToken),
};

// create a Client instance with custom configuration
const taskclient = new Client(config);

// Client subscription
async function sendServiceRequestWorker(){
    return new Promise((resolve, reject) => {
        // Subscribe to a topic and process messages
        taskclient.subscribe(topic, async function({ task, taskService }) {        
            
            if (task.variables.get('heartbeat') == 'true') {
                wlogger.debug('Got heartbeat');
                wlogger.debug('Former timestamp: ' + requestHeartbeatTimestamp);
                requestHeartbeatTimestamp = new Date();
                taskService.complete(task);
            } else {
                try{
                    wlogger.verbose(`Processing a message`);
                    //Create the request
                    var request = await CreateServiceRequest.createServiceRequest(task, wlogger);
                    wlogger.verbose(`Transformed request: ${JSON.stringify(request)}`);         
                    //Post the request
                    var result = await sendRequest(request);
                    //Close the task
                    const processVariables = new Variables().set('sendRequestResult', result);
                    taskService.complete(task, processVariables); ;  
                }catch (error) {
                    var errMSG = (error.errorMessage != undefined) ? error.errorMessage : error;
                    wlogger.warn(`Caught error ${errMSG}`);
                    var retries = (error.retries != undefined) ? error.retries : 0;
                    taskService.handleFailure(task, {
                        errorMessage: errMSG,
                        errorDetails: error.errorDetails,
                        retries: retries,
                        retryTimeout: retryDelay
                    });
                }
            }
        });
    });
}

async function sendRequest(request){
    return new Promise((resolve, reject) => {
        
        // Initialize Rest Client
        var options_auth = { user: snUsername, password: snPassword };
        var restclient = new RestClient(options_auth);
        const reqHeaders = {
            'Accept' : 'application/json',
            'Content-Type' : 'application/json',
        };

        //Set arguments
        var args = {
            path: {
                serviceNowBaseUrl: snBaseUrl,
                catalogitem_sys_id: request.catalogitem_sys_id,
            },
            headers: reqHeaders,
            data: request.requestbody,

        };

        //Do the invoke
        wlogger.verbose("Posting request to Servicenow via rest API");
        restclient.post('https://${serviceNowBaseUrl}/api/sn_sc/servicecatalog/items/${catalogitem_sys_id}/order_now', 
        args, function (data, response) {
            // Got a response  
            wlogger.verbose('Received HTTP status: ' + response.statusCode);
            wlogger.verbose('Received HTTP response: ' + JSON.stringify(data));
            if(response.statusCode === 200 || response.statusCode === 201 || response.statusCode === 204){   
                //Check that the data is ok
                if(data.result != undefined && data.result.request_number != undefined){
                    resolve(data.result);
                }else{
                    // request number missing, throw an error
                    var error = {
                        errorMessage: 'Request number not found in response',
                        errorDetails: JSON.stringify(data),
                        retries: 0,
                    }
                    reject(error);
                }
            }else if (response.statusCode === 400 || response.statusCode === 500){
                wlogger.error('Received HTTP status: ' + response.statusCode);
                var error = {
                    errorMessage: "Received HTTP Statuscode " + response.statusCode,
                    errorDetails: JSON.stringify(data),
                    retries: 0,
                }
                reject(error);
            }else{     
                //Retriable fault
                wlogger.error('Received HTTP status: ' + response.statusCode + " Will retry.");
                var error = {
                    errorMessage: "Received HTTP Statuscode " + response.statusCode,
                    errorDetails: JSON.stringify(data),
                    retries: 1,
                }
                reject(error);
            }
          
        }).on('error', function (err) {
            wlogger.error('Request failed: ' + err.message);
            wlogger.verbose('Request options: ' + JSON.stringify(err.request.options));
            var error = {
                errorMessage: err.message,
                errorDetails: err.name,
                retries: 1,
            };
            reject(error);
        });

    });
}

module.exports = {sendServiceRequestWorker};