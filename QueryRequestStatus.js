/* 

Module reposible of querying service request status from ServiceNow

*/

const { Client, logger, Variables } = require('camunda-external-task-client-js');
require('dotenv').config();
const winston = require('winston');
const { combine, timestamp, label, printf } = winston.format;
const serviceNowTableApi = require('./ServiceNowTableApi');
const BearerTokenAuthInterceptor = require('vasara-bearer-token-interceptor');

// Environment variables
const logLevel = process.env.LOGGING_LEVEL || 'info';
const engineUrl = process.env.BPMENGINEURL || 'http://localhost:8080/engine-rest';
const topic = process.env.QUERYREQUESTSTATUSTOPIC  || 'queryRequestStatus';
const taskClientTimeout = process.env.TASKCLIENTTIMEOUT || 10000;
const retryDelay = process.env.RETRYDELAY || 1000;
const workerId = process.env.QUERYREQUESTSTATUSWORKERID  || 'some-random-id';
const authToken = process.env.AUTHORIZATION_TOKEN || '1234';

//Create a logger
const wloggerFormat = printf(({ level, message, label, timestamp }) => {
    return `${timestamp} [${label}] ${level}: ${message}`;
});

const wlogger = winston.createLogger({
    level: logLevel,
    format: combine(
      label({ label: 'QueryRequestStatus' }),
      timestamp(),
      wloggerFormat,
    ),
    transports: [
      new winston.transports.Console(),
    ],
});

// Create Camunda task client configuration
//todo lisää interceptor
const config = { 
    baseUrl: engineUrl, 
    use: logger.level((logLevel ==='http') ? 'info' : logLevel), 
    asyncResponseTimeout: taskClientTimeout,
    workerId: workerId,
    interceptors: new BearerTokenAuthInterceptor(authToken),
};


// create a Client instance with custom configuration
const taskclient = new Client(config);

// Client subscription
async function queryRequestStatusWorker(){
    return new Promise((resolve, reject) => {
        
        // Subscribe to a topic and process messages
        taskclient.subscribe(topic, async function({ task, taskService }) {
            if (task.variables.get('heartbeat') == 'true') {
                wlogger.debug('Got heartbeat');
                wlogger.debug('Former timestamp: ' + queryHeartbeatTimestamp);
                queryHeartbeatTimestamp = new Date();
                taskService.complete(task);
            } else {
                try {
                    wlogger.verbose(`Processing a message`);
                    //Query service status
                    var serviceRequestStatus = await queryRequestStatus(task);
                    var allItemsComplete = true;
                    for(i in serviceRequestStatus){
                        if(serviceRequestStatus[i].state != 3){
                            allItemsComplete = false;
                        }
                    }
                    wlogger.verbose('Is the request complete: ' + allItemsComplete);
                    const processVariables = new Variables().setAll({
                        serviceRequestStatus: serviceRequestStatus,
                        requestComplete: allItemsComplete,
                    });
                    taskService.complete(task, processVariables); ;  
                }catch (error){
                    var errMSG = (error.errorMessage != undefined) ? error.errorMessage : error;
                    wlogger.warn(`Caught error ${errMSG}`);
                    var retries = (error.retries != undefined) ? error.retries : 0;
                    taskService.handleFailure(task, {
                        errorMessage: errMSG,
                        errorDetails: error.errorDetails,
                        retries: retries,
                        retryTimeout: retryDelay
                    });
                }
            }
        });

    });
}

async function queryRequestStatus(task){
        // create the keys
        var sendRequestResult = task.variables.get('sendRequestResult');
        var keys = 
        [ 
            {
                keyName: 'request.number',
                keyOperator: '=',
                keyValue: sendRequestResult.request_number,
            }
        ]

        var data = await serviceNowTableApi.qyerySnTable(wlogger, 'sc_req_item', keys);

        if(data.result[0] != undefined){
            return data.result;
        }else {
            // Not found, need to throw an error
            var error = {
                errorMessage: 'Requested information not found',
                errorDetails: 'Table: sc_req_item, Keys: ' + JSON.stringify(keys), 
                retries: 0,
            }
            throw error;
        } 
        
}

module.exports = {queryRequestStatusWorker};