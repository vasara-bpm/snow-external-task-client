FROM node:14.15.1-alpine AS NODEJS_TOOLCHAIN
RUN apk add --update git
WORKDIR /app
ADD package.json package-lock.json ./
RUN npm ci

FROM node:14.15.1-slim
COPY --from=NODEJS_TOOLCHAIN /app/node_modules /app/node_modules
WORKDIR /app
ADD *.js ./
USER nobody
ENTRYPOINT ["node", "ServiceNowExternalTaskClient"]
